# Objectif / Croquis
![croquis reéalisé avec The GIMP](croquis.png)

# Rendu intermédiaire
![rendu intermédiaire](tangerine_intermediaire.png)

# Rendu final
![rendu final](tangerine.png)
